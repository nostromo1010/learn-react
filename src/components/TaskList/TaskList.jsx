import React, {useState, useCallback} from 'react';
import './TaskList.css';
import Button from '../Button/Button';

const TaskList = ({ taskList, onAddTask, updateTaskDoneFlag }) => {
  const [newTaskTitle, setNewTaskTitle] = useState('');

  const onSubmit = useCallback(() => {
    onAddTask(newTaskTitle);
    setNewTaskTitle('');
  }, [newTaskTitle, onAddTask]);

  const onInputChange = useCallback((e) => {
    setNewTaskTitle(e.target.value);
  }, []);

  return (
    <div className="TaskList">
      {taskList.map((task, i) => (
        <div key={i} className="TaskListItem">
          <span>{ task.title }</span>
          <input type="checkbox" value={task.done} onChange={(e) => updateTaskDoneFlag(i, e.target.checked)} />
        </div>
      ))}
      <input type="text" value={newTaskTitle} onChange={onInputChange} />
      <Button onClick={onSubmit}>
        Add new task
      </Button>
    </div>
  )
};

export default TaskList;