import React, { useCallback, useState, useEffect } from 'react';

// функция, React-component
const Counter = ({ stepValue, onCounterChange }) => { // тоже самое что и function() {...}
  const [count, setCount] = useState(0);
  // Объявление стейта
  // useState возвращает массив [
  //   текущее значение стейта,
  //   сет-функция (которая относится к этому стейту, она изменяет стейт и вызывает последующий ререндер)
  // ]
  // 1 рендер, count => 0

  useEffect(() => {
    onCounterChange(count);
  }, [count]);

  const decrease = useCallback(() => {
    setCount((value) => value - stepValue);
    // set-функция принимает либо value, либо функцию,
    // которая на вход принимает текущее значение стейта, и должна вернуть новое значение
  }, [stepValue]);
  // useCallback - метод, который принимает первым аргументом функцию, а вторым массив зависимостей,
  // и мемоизирует эту функцию
  // функция - наш колбэк, который где-то вызывается (в данном случае на событие click)
  // массив зависимостей - здесь мы указывает при изменении каких данных нам нужно пересоздать коллбэк

  const increase = useCallback(() => {
    setCount((value) => value + stepValue);
  }, [stepValue]);

  return (
    <div>
      Counter Component
      <div>
        <button onClick={decrease}>-</button>
        { count }
        <button onClick={increase}>+</button>
        { count >= 10 ?
          <img src="https://i.ytimg.com/vi/9221veYwPt4/sddefault.jpg" /> :
          <img src="https://i.ytimg.com/vi/nXiAosV4zpU/sddefault.jpg" />
        }
      </div>
    </div>
  );
}

export default Counter;