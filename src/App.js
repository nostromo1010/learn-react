import './App.css';
import Counter from './components/Counter/Counter';
import FullName from './components/FullName/FullName';
import Button from './components/Button/Button';
import TaskList from './components/TaskList/TaskList';
import { useState } from 'react';

const TASK_LIST = [
  { title: 'Сходить в магазин', done: false },
  { title: 'Сходить за пивом', done: false },
  { title: 'Выпить пиво', done: false },
];

function App() {
  const [taskList, setTaskList] = useState(TASK_LIST);

  const onCounterChange = (value) => {
    console.log('=====', value);
  }

  const onButtonClick = () => {
    console.log('===== click on button');
  }

  const onTaskAddClick = (title) => {
    setTaskList(value => [...value, { title, done: false }]);
  }

  const updateTaskDoneFlag = (i, checked) => {
    setTaskList(value => value.map((item, index) => {
      if (index === i) {
        return { ...item, done: checked };
      }

      return item;
    }));
  }

  return (
    <div className="App">
      <Counter stepValue={5} onCounterChange={onCounterChange} />
      <FullName />

      <Button onClick={onButtonClick}>
        Click on me
      </Button>

      <TaskList
        taskList={taskList}
        onAddTask={onTaskAddClick}
        updateTaskDoneFlag={updateTaskDoneFlag}
      />
    </div>
  );
}

export default App;
